package com.api.v1.training.coursetest.coursecontrollertest;


import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.api.v1.training.course.controllers.CourseController;
import com.api.v1.training.course.repository.CourseRepository;
import com.api.v1.training.course.repository.CourseVersionRepository;
import com.api.v1.training.course.request.CourseRequest;
import com.api.v1.training.course.response.ResponseData;
import com.api.v1.training.course.service.CourseService;
import org.junit.platform.runner.JUnitPlatform;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)

 class CourseControllerTest {
	@InjectMocks
	private CourseController courseController;
	
	@Mock
	private CourseRepository courseRepository;
	
	@Mock
	private CourseVersionRepository courseVersionRepository;
	
	@Mock
	private CourseService courseService;
	private static CourseRequest courseRequest;

	@BeforeAll
	public static void setUp() {
		
		courseRequest = new CourseRequest("active","contents","coursename","courseIndex","platform", 300);
	}

	@Test
	   void testAddCourse() {
		MockHttpServletRequest request = new MockHttpServletRequest();
		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
		
		ResponseData response=courseController.addCourse(courseRequest);
		
		//when(courseService.addCourse(any(CourseRequest.class))).then((Answer<ResponseData>) response);
		assertEquals(HttpStatus.CREATED, response.getStatus());
		//assertThat(response.getStatus()).isEqualTo(HttpStatus.CREATED);
	}
	
	@Test
	   void testCourse() {
		MockHttpServletRequest request = new MockHttpServletRequest();
		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
		
		ResponseData response=courseController.addCourse(courseRequest);
		
		//when(courseService.addCourse(any(CourseRequest.class))).then((Answer<ResponseData>) response);
		assertEquals(HttpStatus.CREATED, response.getStatus());
		//assertThat(response.getStatus()).isEqualTo(HttpStatus.CREATED);
	}
	
	
	
	@AfterAll
	public static void clear(){
		courseRequest=null;
	}
}


