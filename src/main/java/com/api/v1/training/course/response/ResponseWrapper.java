package com.api.v1.training.course.response;

import org.springframework.http.HttpStatus;

public class ResponseWrapper<T> extends ResultSet<T> {

	@SuppressWarnings("unchecked")
	public ResponseWrapper(T t, HttpStatus status) {
		super((T) new ResultSet<>(t, status), status);
	}
	
	public ResponseWrapper(String msg, HttpStatus status) {
		super(msg, status);
	}
	

}
