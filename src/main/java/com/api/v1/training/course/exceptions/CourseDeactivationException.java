package com.api.v1.training.course.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
public class CourseDeactivationException extends CourseException {

	private static final long serialVersionUID = 1L;
	
	public CourseDeactivationException() {
		super();
	}
	
	public CourseDeactivationException(String message) {
		super(message);
	}
	
	public CourseDeactivationException(String message, Throwable cause) {
		super(message, cause);
	}


}
