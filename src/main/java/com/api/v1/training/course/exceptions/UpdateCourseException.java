package com.api.v1.training.course.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
public class UpdateCourseException extends CourseException{

	private static final long serialVersionUID = 1L;
	
	public UpdateCourseException() {
		super();
	}
	
	public UpdateCourseException(String message) {
		super(message);
	}
	
	public UpdateCourseException(String message, Throwable cause) {
		super(message, cause);
	}
	

}
