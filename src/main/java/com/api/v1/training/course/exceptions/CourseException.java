package com.api.v1.training.course.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
public class CourseException extends TrainingSiteApplicationException{


	private static final long serialVersionUID = 1L;
	
	public CourseException() {
		super();
	}
	
	public CourseException(String message) {
		super(message);
	}
	
	public CourseException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
