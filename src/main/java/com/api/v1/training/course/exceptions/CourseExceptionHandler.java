package com.api.v1.training.course.exceptions;


import javax.servlet.ServletException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.api.v1.training.course.response.ErrorData;

	
@ControllerAdvice
public class CourseExceptionHandler extends ResponseEntityExceptionHandler {
	
	@ExceptionHandler({ Exception.class, ServletException.class })
	public ResponseEntity<Object> genericException(Exception ex) {
		ex.printStackTrace();
		ErrorData errorData = createError(HttpStatus.INTERNAL_SERVER_ERROR, ex);
		errorData.setMessage(ex.getLocalizedMessage());
		return new ResponseEntity<>(errorData, errorData.getStatus());
	}

	private ErrorData createError(HttpStatus status, Exception e) {
		ErrorData errorData = new ErrorData(status);
		errorData.setDebugMessage(e.getMessage());
		return errorData;
	}
}
