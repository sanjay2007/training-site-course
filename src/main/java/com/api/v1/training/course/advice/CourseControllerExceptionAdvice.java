package com.api.v1.training.course.advice;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import com.api.v1.training.course.exceptions.AddCourseException;
import com.api.v1.training.course.exceptions.CourseAlreadyExistException;
import com.api.v1.training.course.exceptions.CourseException;
import com.api.v1.training.course.exceptions.CourseNotFoundException;
import com.api.v1.training.course.exceptions.CourseRequestArgumentNotValidException;
import com.api.v1.training.course.response.ExceptionMessage;

@RestControllerAdvice
public class CourseControllerExceptionAdvice {

	@ExceptionHandler(Exception.class)
	public ResponseEntity<Object> genericException(Exception ex, WebRequest requset) {

		ExceptionMessage exceptionMessageObj = new ExceptionMessage(LocalDateTime.now());

		// Handle All Field Validation Errors //MethodArgumentTypeMismatchException //IllegalArgumentException 
		if (ex instanceof MethodArgumentNotValidException||ex instanceof ClassCastException || ex instanceof MethodArgumentTypeMismatchException ||ex instanceof IllegalArgumentException) {
			StringBuilder sb = new StringBuilder();
			List<FieldError> fieldErrors = ((MethodArgumentNotValidException) ex).getBindingResult().getFieldErrors();
			for (FieldError fieldError : fieldErrors) {
				sb.append(fieldError.getDefaultMessage());
				sb.append(";");
			}
			ex = new CourseRequestArgumentNotValidException();
			exceptionMessageObj.setMessage(sb.toString());
			exceptionMessageObj.setStatus(HttpStatus.BAD_REQUEST);
		}
		else if (ex instanceof CourseNotFoundException) {
			exceptionMessageObj.setMessage(ex.getLocalizedMessage());
			exceptionMessageObj.setStatus(HttpStatus.NOT_FOUND);
		}
		else if(ex instanceof DataIntegrityViolationException||ex instanceof CourseAlreadyExistException) {
			exceptionMessageObj.setMessage("Course with the given course name already exist");
			exceptionMessageObj.setStatus(HttpStatus.CONFLICT);
		}
		//TO-DO Add remaining custom exceptions  
		// Handle all the Exceptions
		else if(ex instanceof AddCourseException||ex instanceof CourseException) {
			exceptionMessageObj.setMessage(ex.getLocalizedMessage());
			exceptionMessageObj.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else {
			exceptionMessageObj.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
			exceptionMessageObj.setMessage(ex.getLocalizedMessage());
		}
		exceptionMessageObj.setError(ex.getClass().getSimpleName());
		exceptionMessageObj.setPath(((ServletWebRequest) requset).getRequest().getServletPath());
		return new ResponseEntity<>(exceptionMessageObj, new HttpHeaders(), exceptionMessageObj.getStatus());
	}
}
