package com.api.v1.training.course.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class CourseRequestArgumentNotValidException extends CourseException{

	private static final long serialVersionUID = 1L;

	public CourseRequestArgumentNotValidException() {
		super();
	}
	
	public CourseRequestArgumentNotValidException(String exception) {
		super(exception);
	}

	public CourseRequestArgumentNotValidException(String exception, Throwable cause) {
		super(exception,cause);
	}
	
}
