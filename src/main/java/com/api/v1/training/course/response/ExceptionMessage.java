package com.api.v1.training.course.response;

import java.time.LocalDateTime;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonFormat;

public class ExceptionMessage {

	private HttpStatus status;
	@JsonFormat( pattern = "dd-MM-yyyy hh:mm:ss")
	private LocalDateTime timestamp;
	private String message;
	private String error;
	private String path;
	
	public ExceptionMessage(LocalDateTime localDateTime) {
		this.timestamp=localDateTime;
	}
	public ExceptionMessage(HttpStatus status, LocalDateTime timestamp, String message, String error, String path) {
		super();
		this.status = status;
		this.timestamp = timestamp;
		this.message = message;
		this.error = error;
		this.path = path;
	}
	public HttpStatus getStatus() {
		return status;
	}
	public void setStatus(HttpStatus status) {
		this.status = status;
	}
	public LocalDateTime getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	
	
}
