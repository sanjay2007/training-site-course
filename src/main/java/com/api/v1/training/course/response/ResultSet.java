package com.api.v1.training.course.response;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.http.HttpStatus;

import com.api.v1.training.course.exceptions.CourseNotFoundException;
import com.fasterxml.jackson.annotation.JsonFormat;


//@Data
public class ResultSet<T> {

	private HttpStatus status;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
	private LocalDateTime timestamp;
	private T data;
	private String message;
	
	private ResultSet()
	{
		this.timestamp = LocalDateTime.now();
	}

	public ResultSet( T o, HttpStatus status )
	{
		this();
		if ( o == null || ( o instanceof List && ( ( List ) o ).isEmpty() ) )
			throw new CourseNotFoundException( "No Content Found" );
		this.status = status;
		this.data = o;
	}
	
	public ResultSet( String msg, HttpStatus status ) { 
		this();
		this.status = status;
		this.message=msg;
	}

}
