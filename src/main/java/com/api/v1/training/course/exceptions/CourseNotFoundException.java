package com.api.v1.training.course.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class CourseNotFoundException extends CourseException {

	private static final long serialVersionUID = 1L;

	public CourseNotFoundException() {
		super();
	}
	
	public CourseNotFoundException(String exception) {
		super(exception);
	}

	public CourseNotFoundException(String exception, Throwable cause) {
		super(exception,cause);
	}
}
