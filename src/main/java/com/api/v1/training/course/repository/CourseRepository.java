package com.api.v1.training.course.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

import com.api.v1.training.course.domain.Course;

@Repository
public interface CourseRepository extends JpaRepository<Course, Integer>{
	
}
