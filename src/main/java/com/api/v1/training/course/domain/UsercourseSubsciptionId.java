package com.api.v1.training.course.domain;
// Generated 27 Jun, 2020 1:10:35 PM by Hibernate Tools 4.3.5.Final

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * UsercourseSubsciptionId generated by hbm2java
 */
@Embeddable
public class UsercourseSubsciptionId implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int userid;
	private int courseid;

	public UsercourseSubsciptionId() {
	}

	public UsercourseSubsciptionId(int userid, int courseid) {
		this.userid = userid;
		this.courseid = courseid;
	}

	@Column(name = "userid", nullable = false)
	public int getUserid() {
		return this.userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	@Column(name = "courseid", nullable = false)
	public int getCourseid() {
		return this.courseid;
	}

	public void setCourseid(int courseid) {
		this.courseid = courseid;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof UsercourseSubsciptionId))
			return false;
		UsercourseSubsciptionId castOther = (UsercourseSubsciptionId) other;

		return (this.getUserid() == castOther.getUserid()) && (this.getCourseid() == castOther.getCourseid());
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + this.getUserid();
		result = 37 * result + this.getCourseid();
		return result;
	}

}
