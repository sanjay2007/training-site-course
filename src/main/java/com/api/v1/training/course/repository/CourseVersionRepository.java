package com.api.v1.training.course.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.api.v1.training.course.domain.Courseversion;
import com.api.v1.training.course.domain.CourseversionId;
public interface CourseVersionRepository extends JpaRepository<Courseversion, CourseversionId> {
	
	/*
	 * @Query("SELECT CourseversionId  FROM Courseversion cv WHERE cv.coursename = ?1 and cv.courseversion = ?2"
	 * ) Courseversion findByCoursenameAndCourseversion(String coursename, Integer courseversion);
	 */
}
