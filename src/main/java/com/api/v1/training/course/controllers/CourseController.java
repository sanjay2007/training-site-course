package com.api.v1.training.course.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.v1.training.course.adapters.CourseInputAdapter;
import com.api.v1.training.course.request.CourseRequest;
import com.api.v1.training.course.response.CourseResponse;
import com.api.v1.training.course.response.ResponseData;
import com.api.v1.training.course.service.CourseService;

import javax.validation.Valid;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "/trainingsite/courses")
@RestController
@RequestMapping("/trainingsite/courses")
public class CourseController {

	@Autowired
	CourseService courseService;
	@Autowired
	CourseInputAdapter  courseInputAdapter;

	// get single course by courseId
	@ApiOperation(value = "Find Course By courseName", notes = "Provide a single courseName as a input in path variable", response = ResponseEntity.class, responseContainer = "CourseResponseObject")
	@GetMapping( value = "/{courseName}")
	public ResponseEntity<CourseResponse> getCourseByID(@PathVariable String courseName) {
		CourseResponse courseResponse = courseService.getCourseByID(courseInputAdapter.adaptCourseNameToCourseId(courseName));
		return ResponseEntity.status(HttpStatus.OK).body(courseResponse);
	}

	// get single course by courseId and course version i.e. CoursevertionID
	@GetMapping(value = "/{courseName}/{courseversion}")
	public ResponseEntity<CourseResponse> getCourseByCourseversionId(@PathVariable(value = "courseName") String courseName,
			@PathVariable(value = "courseversion") Integer courseversion) {
		CourseResponse courseResponse = courseService.getCourseByCouseversionID(courseInputAdapter.adaptCourseNameToCourseId(courseName), courseversion);
		return ResponseEntity.status(HttpStatus.OK).body(courseResponse);
	}

	// get all active courses ()
	@GetMapping(value = "/active")
	public ResponseEntity<List<CourseResponse>> getAllActiveCourses() {
		List<CourseResponse> courseResponseList = courseService.getAllActiveCourses();
		return ResponseEntity.status(HttpStatus.OK).body(courseResponseList);
	}

	// add course
	@PostMapping(value = "/")
	public ResponseData addCourse(@Valid @RequestBody CourseRequest request) {
		String message = courseService.addCourse(request);
		return new ResponseData(HttpStatus.OK, message);
	}

	// Update course
	@PostMapping(value = "/{courseName}")
	public ResponseData updateCourse(@PathVariable(value = "courseName") String courseName, @Valid @RequestBody CourseRequest request) {
		String message = courseService.updateCourse(courseInputAdapter.adaptCourseNameToCourseId(courseName), request);
		return new ResponseData(HttpStatus.ACCEPTED, message);
	}

	// Activate Course
	@PutMapping(value = "/activateCourse/{courseName}/{courseStatus}")
	public ResponseData activateCourse(@PathVariable(value = "courseName") String courseName,@PathVariable(value = "courseStatus") String courseStatus) {
		String message = courseService.activateCourse(courseInputAdapter.adaptCourseNameToCourseId(courseName), courseStatus);
		return new ResponseData(HttpStatus.ACCEPTED, message);
	}

		
	// Deactivate Course
		@PutMapping(value = "/deactivateCourse/{courseName}/{courseStatus}")
		public ResponseData deactivateCourse(@PathVariable(value = "courseName") String courseName,@PathVariable(value = "courseStatus") String courseStatus) {
			String message = courseService.deactivateCourse(courseInputAdapter.adaptCourseNameToCourseId(courseName), courseStatus);
			return new ResponseData(HttpStatus.ACCEPTED, message);
		}
}
