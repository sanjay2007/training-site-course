package com.api.v1.training.course.serviceimpl;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.api.v1.training.course.constants.ApplConstants;
import com.api.v1.training.course.domain.Course;
import com.api.v1.training.course.domain.Courseversion;
import com.api.v1.training.course.domain.CourseversionId;
import com.api.v1.training.course.exceptions.AddCourseException;
import com.api.v1.training.course.exceptions.CourseActivationException;
import com.api.v1.training.course.exceptions.CourseAlreadyExistException;
import com.api.v1.training.course.exceptions.CourseDeactivationException;
import com.api.v1.training.course.exceptions.CourseException;
import com.api.v1.training.course.exceptions.CourseNotFoundException;
import com.api.v1.training.course.exceptions.UpdateCourseException;
import com.api.v1.training.course.repository.CourseRepository;
import com.api.v1.training.course.repository.CourseVersionRepository;
import com.api.v1.training.course.request.CourseRequest;
import com.api.v1.training.course.response.CourseResponse;
import com.api.v1.training.course.service.CourseService;

@Transactional
@Service
public class CourseServiceImpl implements CourseService {

	@Autowired
	private CourseRepository courseRepository;

	@Autowired
	private CourseVersionRepository courseVersionReository;
	CourseResponse courseResponse;

	@Override
	public CourseResponse getCourseByID(Integer courseId) {
		try {
			if (!courseRepository.existsById(courseId))
				throw new CourseNotFoundException("Course Doesnot Exist.");
			Course course = courseRepository.getOne(courseId);
			courseResponse = new CourseResponse();
			courseResponse.setCourse(course);
			courseResponse.setCourseVersion(
					courseVersionReository.getOne(new CourseversionId(courseId, course.getCourseversion())));
		} catch (CourseNotFoundException e) {
			throw e;
		} catch (Exception e) {
			throw new CourseException("Exception in get course by id", e);
		}
		return courseResponse;
	}

	@Override
	public CourseResponse getCourseByCouseversionID(Integer courseId, Integer courseversion) {
		try {
			if (!courseRepository.existsById(courseId)) {
				throw new CourseNotFoundException("Course with the given course id does not exist");
			}
			if (!courseVersionReository.existsById(new CourseversionId(courseId, courseversion))) {
				throw new CourseNotFoundException("Course with the given course version does not exist");
			}
			courseResponse = new CourseResponse();
			courseResponse
			.setCourseVersion(courseVersionReository.getOne(new CourseversionId(courseId, courseversion)));
		} catch (CourseNotFoundException e) {
			throw e;
		} catch (Exception e) {
			throw new CourseException("Exception in get course by course version id.", e);
		}
		return courseResponse;
	}

	@Override
	public List<CourseResponse> getAllActiveCourses() {
		List<CourseResponse> listCourseResponses = new ArrayList<>();
		try {
			List<Course> courses = courseRepository.findAll();
			Predicate<Course> activeCoursePredicate = p -> p.getCoursestatus()
					.equalsIgnoreCase(ApplConstants.COURSE_STATUS_ACTIVE);
			List<Course> activeCourseList = courses.stream().filter(activeCoursePredicate).collect(Collectors.toList());
			Consumer<Course> constructCourseResponseAction = a -> {
				courseResponse = new CourseResponse();
				courseResponse.setCourse(a);
				courseResponse.setCourseVersion(
						courseVersionReository.getOne(new CourseversionId(a.getCourseid(), a.getCourseversion())));
				listCourseResponses.add(courseResponse);
			};
			activeCourseList.forEach(constructCourseResponseAction);
		} catch (CourseNotFoundException e) {
			throw e;
		} catch (Exception e) {
			throw new CourseException("Exception in get all active courses", e);
		}
		return listCourseResponses;
	}

	@Override
	public String addCourse(CourseRequest request) {
		
		try {
			Course course = new Course();
			course.setCoursestatus(request.getCoursestatus());
			course.setCourseversion(1);
			course.setCoursename(request.getCoursename());
			course = courseRepository.save(course);

			Courseversion courseversion = new Courseversion();
			CourseversionId courseversionId = new CourseversionId(course.getCourseid(), 1);
			courseversion.setId(courseversionId);
			courseversion.setCourse(course);
			courseversion.setCoursename(request.getCoursename());
			courseversion.setCourseIndex(request.getCourseIndex());
			courseversion.setPlatform(request.getPlatform());
			courseversion.setContents(request.getContents());
			courseversion.setPrice(request.getPrice());
			courseVersionReository.save(courseversion);
		}catch (DataIntegrityViolationException e) {
			throw new CourseAlreadyExistException("Course with the given coursename already exist");
		} catch (Exception e) {
			throw new AddCourseException("Exception in add course.", e);
		}
		return "Course Added Successfuly.";
	}

	@Override
	public String updateCourse(Integer courseId, CourseRequest request) {
		try {
			if (!courseRepository.existsById(courseId)) {
				throw new CourseNotFoundException("Course Does not Exists.");
			}
			Course course = courseRepository.getOne(courseId);
			int newCourseversionNo = findNextcourseId(courseId);

			course.setCourseversion(newCourseversionNo);
			course.setCoursestatus(request.getCoursestatus());
			course.setCoursename(request.getCoursename());

			Courseversion newCourseversion = new Courseversion();
			CourseversionId courseversionId = new CourseversionId(courseId, newCourseversionNo);
			newCourseversion.setId(courseversionId);
			newCourseversion.setCourse(course);
			newCourseversion.setCoursename(request.getCoursename());
			newCourseversion.setCourseIndex(request.getCourseIndex());
			newCourseversion.setPlatform(request.getPlatform());
			newCourseversion.setContents(request.getContents());
			newCourseversion.setPrice(request.getPrice());
			courseVersionReository.save(newCourseversion);
		} catch (CourseNotFoundException e) {
			throw e;
		} catch (Exception e) {
			throw new UpdateCourseException("Exception in update course.", e);
		}
		return "Course Updated Successfully.";
	}

	// Utility method to find next coursevertion in case of updateCourse
	private Integer findNextcourseId(Integer courseId) {
		return courseRepository.getOne(courseId).getCourseversion() + 1;
	}

	@Override
	public String activateCourse(Integer courseId, String newCourseStatus) {
		String courseStatus = "";
		try {
			if (!newCourseStatus.equalsIgnoreCase(ApplConstants.COURSE_STATUS_ACTIVE)) {
				throw new IllegalArgumentException(
						"Course status (" + newCourseStatus + ") is not a valid status for this api.");
			}
			Course course = courseRepository.getOne(courseId);
			String oldCourseStatus = course.getCoursestatus();
			if (oldCourseStatus.equals(newCourseStatus)) {
				courseStatus = "Course is already active";
			} else {
				course.setCoursestatus(newCourseStatus);
				courseStatus = "Course is activated";
			}
		} catch (IllegalArgumentException e) {
			throw e;
		} catch (Exception e) {
			throw new CourseActivationException("Exception in activate course.", e);
		}
		return courseStatus;
	}

	@Override
	public String deactivateCourse(Integer courseId, String newCourseStatus) {
		String courseStatus = "";
		try {
			if (!newCourseStatus.equalsIgnoreCase(ApplConstants.COURSE_STATUS_DEACTIVE)) {
				throw new IllegalArgumentException(
						"Course status (" + newCourseStatus + ") is not a valid status for this api.");
			}
			Course course = courseRepository.getOne(courseId);
			String oldCourseStatus = course.getCoursestatus();
			if (oldCourseStatus.equals(newCourseStatus)) {
				courseStatus = "Course is already deactive";
			} else {
				course.setCoursestatus(newCourseStatus);
				courseStatus = "Course is deactivated";
			}
		} catch (IllegalArgumentException e) {
			throw e;
		} catch (Exception e) {
			throw new CourseDeactivationException("Exception in deactivate course.", e);
		}
		return courseStatus;
	}

}
