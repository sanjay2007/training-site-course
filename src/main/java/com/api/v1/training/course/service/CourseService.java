package com.api.v1.training.course.service;


import java.util.List;

import com.api.v1.training.course.request.CourseRequest;
import com.api.v1.training.course.response.CourseResponse;

public interface CourseService {
	
	CourseResponse getCourseByID(Integer courseId);

	String addCourse(CourseRequest request);
	
	String updateCourse(Integer courseId, CourseRequest request);

	String activateCourse(Integer courseId, String courseStatus);
	
	String deactivateCourse(Integer courseId, String courseStatus);

	CourseResponse getCourseByCouseversionID(Integer courseId, Integer courseversion);

	List<CourseResponse> getAllActiveCourses();

}
