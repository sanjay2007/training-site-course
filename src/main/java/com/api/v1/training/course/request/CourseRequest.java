package com.api.v1.training.course.request;

import javax.validation.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;

public class CourseRequest {

	@NotEmpty(message = "Course status must not be empty")
	private String coursestatus;
	@NotEmpty(message = "Course contents must not be empty")
	private String contents;
	@NotEmpty(message = "Course name must not be empty")
	private String coursename;
	@NotEmpty(message = "Course index must not be empty")
	private String courseIndex;
	@NotEmpty(message = "Course platform must not be empty")
	private String platform;
	@Range(min=0)
	private double price;

	public CourseRequest() {
		super();
	}

	public CourseRequest(String coursestatus, String contents, String coursename, String courseIndex, String platform,
			double price) {
		super();
		this.coursestatus = coursestatus;
		this.contents = contents;
		this.coursename = coursename;
		this.courseIndex = courseIndex;
		this.platform = platform;
		this.price = price;
	}

	public String getCoursestatus() {
		return coursestatus;
	}

	public void setCoursestatus(String coursestatus) {
		this.coursestatus = coursestatus;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public String getCoursename() {
		return coursename;
	}

	public void setCoursename(String coursename) {
		this.coursename = coursename;
	}

	public String getCourseIndex() {
		return courseIndex;
	}

	public void setCourseIndex(String courseIndex) {
		this.courseIndex = courseIndex;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
}
