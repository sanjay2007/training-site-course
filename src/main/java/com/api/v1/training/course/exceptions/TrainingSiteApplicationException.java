package com.api.v1.training.course.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
public class TrainingSiteApplicationException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public TrainingSiteApplicationException() {
		super();
	}
	
	public TrainingSiteApplicationException(String message) {
		super(message);
	}
	
	public TrainingSiteApplicationException(String message, Throwable cause) {
		super(message, cause);
	}
	
	
}
