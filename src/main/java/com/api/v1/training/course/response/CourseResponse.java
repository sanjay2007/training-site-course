package com.api.v1.training.course.response;


import com.api.v1.training.course.domain.Course;
import com.api.v1.training.course.domain.Courseversion;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CourseResponse {

	private Course course;
	private Courseversion courseVersion;
	public Course getCourse() {
		return course;
	}
	public void setCourse(Course course2) {
		this.course = course2;
	}
	public Courseversion getCourseVersion() {
		return courseVersion;
	}
	public void setCourseVersion(Courseversion courseversion2) {
		this.courseVersion = courseversion2;
	}
}
