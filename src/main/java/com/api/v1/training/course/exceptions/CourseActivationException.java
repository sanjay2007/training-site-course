package com.api.v1.training.course.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
public class CourseActivationException  extends CourseException{

	private static final long serialVersionUID = 1L;
	
	public CourseActivationException() {
		super();
	}
	
	public CourseActivationException(String message) {
		super(message);
	}
	
	public CourseActivationException(String message, Throwable cause) {
		super(message, cause);
	}
}
