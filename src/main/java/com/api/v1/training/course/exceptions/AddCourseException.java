package com.api.v1.training.course.exceptions;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
public class AddCourseException extends CourseException{

	private static final long serialVersionUID = 1L;

	public AddCourseException() {
		super();
	}
	
	public AddCourseException(String message) {
		super(message);
	}
	
	public AddCourseException(String message, Throwable cause) {
		super(message, cause);
	}
}
