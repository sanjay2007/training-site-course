package com.api.v1.training.course.adapters;

import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.api.v1.training.course.domain.Course;
import com.api.v1.training.course.exceptions.CourseNotFoundException;
import com.api.v1.training.course.repository.CourseRepository;

@Component
public class CourseInputAdapter {

	@Autowired
	private CourseRepository courseRepository;
	
	public Integer adaptCourseNameToCourseId(String courseName) {
		List <Course>course =courseRepository.findAll().stream().filter(p->p.getCoursename().equalsIgnoreCase(courseName)).collect(Collectors.toList());
		if(course.isEmpty()) {
			throw new CourseNotFoundException("Course not found with the given course name.");
		}
		 return course.get(0).getCourseid();
	}
	
	
	
}
