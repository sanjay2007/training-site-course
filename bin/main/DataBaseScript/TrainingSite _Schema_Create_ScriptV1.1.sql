SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;

SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';



DROP SCHEMA IF EXISTS `trainingsite` ;

CREATE SCHEMA IF NOT EXISTS `trainingsite` DEFAULT CHARACTER SET utf8 ;

SHOW WARNINGS;

USE `trainingsite` ;



-- -----------------------------------------------------

-- Table `trainingsite`.`course`

-- -----------------------------------------------------

DROP TABLE IF EXISTS `trainingsite`.`course` ;



SHOW WARNINGS;

CREATE  TABLE IF NOT EXISTS `trainingsite`.`course` (

  `courseid` INT(11) NOT NULL ,

  `courseversion` INT(11) NULL DEFAULT NULL ,

  `coursestatus` VARCHAR(45) NULL DEFAULT NULL ,

  PRIMARY KEY (`courseid`) )

ENGINE = InnoDB

DEFAULT CHARACTER SET = utf8;



SHOW WARNINGS;



-- -----------------------------------------------------

-- Table `trainingsite`.`courseversion`

-- -----------------------------------------------------

DROP TABLE IF EXISTS `trainingsite`.`courseversion` ;



SHOW WARNINGS;

CREATE  TABLE IF NOT EXISTS `trainingsite`.`courseversion` (

  `courseid` INT(11) NOT NULL ,

  `courseversion` INT(11) NOT NULL ,

  `coursename` VARCHAR(45) NULL DEFAULT NULL ,

  `platform` VARCHAR(45) NULL DEFAULT NULL ,

  `price` DOUBLE NULL DEFAULT NULL ,

  `courseindex` VARCHAR(45) NULL DEFAULT NULL ,

  `contents` LONGTEXT NULL DEFAULT NULL ,

  PRIMARY KEY (`courseid`, `courseversion`) ,

  CONSTRAINT `courseversion_ibfk_2`

    FOREIGN KEY (`courseid` )

    REFERENCES `trainingsite`.`course` (`courseid` ))

ENGINE = InnoDB

DEFAULT CHARACTER SET = utf8;



SHOW WARNINGS;



-- -----------------------------------------------------

-- Table `trainingsite`.`hibernate_sequence`

-- -----------------------------------------------------

DROP TABLE IF EXISTS `trainingsite`.`hibernate_sequence` ;



SHOW WARNINGS;

CREATE  TABLE IF NOT EXISTS `trainingsite`.`hibernate_sequence` (

  `next_val` BIGINT(20) NULL DEFAULT NULL )

ENGINE = MyISAM

DEFAULT CHARACTER SET = utf8;



SHOW WARNINGS;



-- -----------------------------------------------------

-- Table `trainingsite`.`role`

-- -----------------------------------------------------

DROP TABLE IF EXISTS `trainingsite`.`role` ;



SHOW WARNINGS;

CREATE  TABLE IF NOT EXISTS `trainingsite`.`role` (

  `roleid` INT(11) NOT NULL ,

  `rolename` VARCHAR(45) NULL DEFAULT NULL ,

  PRIMARY KEY (`roleid`) )

ENGINE = InnoDB

DEFAULT CHARACTER SET = utf8;



SHOW WARNINGS;



-- -----------------------------------------------------

-- Table `trainingsite`.`user`

-- -----------------------------------------------------

DROP TABLE IF EXISTS `trainingsite`.`user` ;



SHOW WARNINGS;

CREATE  TABLE IF NOT EXISTS `trainingsite`.`user` (

  `userid` INT(11) NOT NULL ,

  `profileversion` INT(11) NULL DEFAULT NULL ,

  `status` VARCHAR(45) NULL DEFAULT NULL ,

  `roleid` INT(11) NOT NULL ,

  `password` VARCHAR(45) NULL DEFAULT NULL ,

  `uniquecode` VARCHAR(45) NULL DEFAULT NULL ,

  `resetpasswordstatus` VARCHAR(45) NULL DEFAULT NULL ,

  `username` VARCHAR(45) NOT NULL ,

  PRIMARY KEY (`userid`) ,

  UNIQUE INDEX `uniquecode_UNIQUE` (`uniquecode` ASC) ,

  UNIQUE INDEX `UKasdhkib9v9c0ota5oiw28tl85` (`uniquecode` ASC) ,

  INDEX `roleid` (`roleid` ASC) ,

  CONSTRAINT `roleid`

    FOREIGN KEY (`roleid` )

    REFERENCES `trainingsite`.`role` (`roleid` )

    ON DELETE NO ACTION

    ON UPDATE NO ACTION)

ENGINE = InnoDB

DEFAULT CHARACTER SET = utf8;



SHOW WARNINGS;



-- -----------------------------------------------------

-- Table `trainingsite`.`usercourse_subsciption`

-- -----------------------------------------------------

DROP TABLE IF EXISTS `trainingsite`.`usercourse_subsciption` ;



SHOW WARNINGS;

CREATE  TABLE IF NOT EXISTS `trainingsite`.`usercourse_subsciption` (

  `userid` INT(11) NOT NULL ,

  `courseid` INT(11) NOT NULL ,

  `courseversion` INT(11) NULL DEFAULT NULL ,

  PRIMARY KEY (`userid`, `courseid`) ,

  INDEX `courseid` (`courseid` ASC) ,

  CONSTRAINT `usercourse_subsciption_ibfk_2`

    FOREIGN KEY (`courseid` )

    REFERENCES `trainingsite`.`course` (`courseid` ),

  CONSTRAINT `usercourse_subsciption_ibfk_1`

    FOREIGN KEY (`userid` )

    REFERENCES `trainingsite`.`user` (`userid` ))

ENGINE = InnoDB

DEFAULT CHARACTER SET = utf8;



SHOW WARNINGS;



-- -----------------------------------------------------

-- Table `trainingsite`.`userprofile`

-- -----------------------------------------------------

DROP TABLE IF EXISTS `trainingsite`.`userprofile` ;



SHOW WARNINGS;

CREATE  TABLE IF NOT EXISTS `trainingsite`.`userprofile` (

  `userid` INT(11) NOT NULL ,

  `profileversion` INT(11) NOT NULL ,

  `firstname` VARCHAR(45) NULL DEFAULT NULL ,

  `lastname` VARCHAR(45) NULL DEFAULT NULL ,

  `city` VARCHAR(45) NULL DEFAULT NULL ,

  `state` VARCHAR(45) NULL DEFAULT NULL ,

  `country` VARCHAR(45) NULL DEFAULT NULL ,

  `email` VARCHAR(45) NULL DEFAULT NULL ,

  `mobile` VARCHAR(45) NULL DEFAULT NULL ,

  `profileupdatestatus` VARCHAR(45) NULL DEFAULT NULL ,

  PRIMARY KEY (`userid`, `profileversion`) ,

  INDEX `userid_idx` (`userid` ASC) ,

  CONSTRAINT `userid`

    FOREIGN KEY (`userid` )

    REFERENCES `trainingsite`.`user` (`userid` )

    ON DELETE NO ACTION

    ON UPDATE NO ACTION)

ENGINE = InnoDB

DEFAULT CHARACTER SET = utf8;



SHOW WARNINGS;





SET SQL_MODE=@OLD_SQL_MODE;

SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

